﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VirtualButtons : MonoBehaviour
{
    public VirtualButtonBehaviour playBtn;
    public VirtualButtonBehaviour pauseBtn;
    public VirtualButtonBehaviour nextBtn;
    public AudioClip[] aClips;
    public AudioSource source;
    public GameObject[] disc;

    public GameObject arm;
    public Animator[] discR;   

    public Animator armAnim;
    bool paused = false;
    bool [] activeDisc = new bool[4];
    int i;
    bool start=false;

    void Start()
    {
        i = 0;
        //start = false;
        foreach (GameObject d in disc)
            d.SetActive(false);
        for (int j = 0; j < activeDisc.Length; j++)
            activeDisc[j] = false;
        playBtn.RegisterOnButtonPressed(OnPlayButtonPressed);
        playBtn.RegisterOnButtonReleased(OnPlayButtonReleased);
        pauseBtn.RegisterOnButtonPressed(OnPauseButtonPressed);
        pauseBtn.RegisterOnButtonReleased(OnPauseButtonReleased);
        nextBtn.RegisterOnButtonPressed(OnNextButtonPressed);
        nextBtn.RegisterOnButtonReleased(OnNextButtonReleased);
        source = GetComponent<AudioSource>();
        source.clip = aClips[0];
    }

    public void OnPlayButtonPressed(VirtualButtonBehaviour Btn)
    {
        if (!start)
        {
            start = true;
            disc[0].SetActive(true);
            activeDisc[0] = true;
            armAnim.Play("arm");
            discR[0].Play("rotate");
        }
        if (paused)
        {
            discR[i].Play("rotate");
            source.Play(0);
            paused = false;

        }
        else
        {
            for(int j=0; j<activeDisc.Length;j++)
                if (activeDisc[j])  discR[j].Play("rotate");
            source.Play();
        }
    }

    public void OnPlayButtonReleased(VirtualButtonBehaviour Btn)
    {
        
    }

    public void OnPauseButtonPressed(VirtualButtonBehaviour Btn)
    {
        for (int j = 0; j < activeDisc.Length; j++)
            if (activeDisc[j])
                {
                    disc[j].SetActive(true);
                    discR[j].Play("New State");
                }
        source.Pause();
        paused = true;
    }

    public void OnPauseButtonReleased(VirtualButtonBehaviour Btn)
    {

    }

    public void OnNextButtonPressed(VirtualButtonBehaviour Btn)
    {
        disc[i].SetActive(false);
        activeDisc[i] = false;
        i = (i + 1) % disc.Length;
        disc[i].SetActive(true);
        activeDisc[i] = true;
        discR[i].Play("rotate");
        source.clip = aClips[i];
        source.Play();
    }

    public void OnNextButtonReleased(VirtualButtonBehaviour Btn)
    {

    }

    void Update()
    {

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsRadio : MonoBehaviour
{
    public AudioClip[] aClips;
    public AudioSource aSource;
    string btnName;


    public float updateStep = 0.1f;
    public int sampleDataLenght = 1024;

    private float currentUpdateTime = 0f;

    public float clipAudio;
    private float[] clipSampleData;

    public GameObject speaker1;
    public GameObject speaker2;
    public float sizeFactor = 1;

    public float minSize = 0;
    public float maxSize = 500;


    // Start is called before the first frame update
    void Start()
    {
        aSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;
            aSource.clip = aClips[0];
            bool paused = false;
            if (Physics.Raycast(ray, out Hit))
            {
                btnName = Hit.transform.name;
                switch (btnName)
                {
                    case "Play":
                        if (paused) aSource.Play(0);
                        else aSource.Play();
                        clipSampleData = new float[sampleDataLenght];
                        break;

                    case "Pause":
                        aSource.Pause();
                        paused = true;
                        break;

                    case "Next":
                        aSource.clip = aClips[1];
                        aSource.Play();
                        break;
                    default:
                        break;
                }
            }
        }

        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            aSource.clip.GetData(clipSampleData, aSource.timeSamples);
            clipAudio = 0f;
            foreach (var sample in clipSampleData)
            {
                clipAudio += Mathf.Abs(sample);
            }
            clipAudio /= sampleDataLenght;

            clipAudio *= sizeFactor;
            clipAudio = Mathf.Clamp(clipAudio, minSize, maxSize);

            speaker1.transform.localScale = new Vector3(clipAudio, clipAudio, clipAudio);
            speaker2.transform.localScale = new Vector3(clipAudio, clipAudio, clipAudio);
        }
    }

}
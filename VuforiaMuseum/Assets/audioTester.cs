using UnityEngine;

public class audioTester : MonoBehaviour
{
    public AudioSource audioSource;
    public float updateStep = 0.1f;
    public int sampleDataLenght = 1024;

    private float currentUpdateTime = 0f;

    public float clipAudio;
    private float[] clipSampleData;

    public GameObject sphere;
    public float sizeFactor = 1;

    public float minSize = 0;
    public float maxSize = 500;

    private void Awake()
    {
        clipSampleData = new float[sampleDataLenght];
    }

    private void Update()
    {
        currentUpdateTime += Time.deltaTime;
        if(currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            audioSource.clip.GetData(clipSampleData, audioSource.timeSamples);
            clipAudio = 0f;
            foreach(var sample in clipSampleData)
            {
                clipAudio += Mathf.Abs(sample);
            }
            clipAudio /= sampleDataLenght;

            clipAudio *= sizeFactor;
            clipAudio = Mathf.Clamp(clipAudio, minSize, maxSize);

            sphere.transform.localScale = new Vector3(clipAudio, clipAudio, clipAudio);

        }
    }

}

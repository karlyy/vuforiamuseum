﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastButtons : MonoBehaviour
{
    public AudioClip[] aClips;
    public AudioSource aSource;
    string btnName;
    public GameObject disk1;
    public GameObject disk2;
    public Animator diskRotation;
    public Animator diskRotation2;

    // Start is called before the first frame update
    void Start()
    {
        aSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount>0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;
            aSource.clip = aClips[0];
            bool paused = false;
            disk1.SetActive(false);
            disk2.SetActive(false);
            bool activeDisk1 = false;
            bool activeDisk2 = false;
            if (Physics.Raycast(ray, out Hit))
            {
                btnName = Hit.transform.name;
                switch (btnName)
                {
                    case "PlaySong":
                        if(activeDisk1==false && activeDisk2 == false)
                        {
                            disk1.SetActive(true);
                            activeDisk1 = true;
                        }
                        diskRotation.Play("rotate");
                        if(paused) aSource.Play(0);
                        else aSource.Play();
                        break;

                    case "PauseSong":
                        diskRotation.Play("Empty");
                        aSource.Pause();
                        paused = true;
                        break;

                    case "NextSong":
                        disk1.SetActive(false);
                        activeDisk1 = false;
                        disk2.SetActive(true);
                        activeDisk2 = true;
                        diskRotation2.Play("rotate");
                        aSource.clip = aClips[1];
                        aSource.Play();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

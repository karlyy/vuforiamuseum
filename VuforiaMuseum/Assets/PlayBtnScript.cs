﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PlayBtnScript : MonoBehaviour
{
    public VirtualButtonBehaviour playBtn;
    public AudioSource song;
    public Animator diskRotation;
    // Start is called before the first frame update
    void Start()
    {
        playBtn.RegisterOnButtonPressed(OnButtonPressed);
        playBtn.RegisterOnButtonReleased(OnButtonReleased);
        song = GetComponent<AudioSource>();
    }

    public void OnButtonPressed(VirtualButtonBehaviour playBtn)
    {
        diskRotation.Play("rotate");
        song.Play();
    }

    public void OnButtonReleased(VirtualButtonBehaviour playBtn)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
